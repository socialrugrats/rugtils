from setuptools import setup

setup(
    name='rugtils',
    version='0.1',
    description='Utilities for SR services',
    url='http://bitbucket.org/socialrugrats/rugtils',
    author='SR Devs',
    author_email='dev@socialrugrats.com',
    license='Proprietary',
    packages=['rugtils'],
    install_requires=[
    	'rfc3339',
    	'strict_rfc3339',
    	'itsdangerous',
    	'redis',
    	'flask-restful',
    	'pytz'
    ],
    zip_safe=False)

