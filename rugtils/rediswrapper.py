import redis


class RedisWrapper():
    def __init__(self, config):
        self.config = config
        self.connection = None

    def connect(self):
        try:
            self.connection = redis.\
                StrictRedis(host=self.config['host'], port=self.config['port'])
        except:
            raise

    def check_connection(self):
        if self.connection is None:
            try:
                self.connect()
            except:
                raise
            if self.connection is None:
                raise RuntimeError('Could not find\
                 connection to redis; check redis server.')

    def get(self, key=None):
        self.check_connection()
        return self.connection.get(key)

    def set(self, key=None, value=None):
        self.check_connection()
        return self.connection.set(key, value)

    def setex(self, key=None, value=None, seconds=60):
        return self.connection.setex(key, seconds, value)

    def zrangebyscore(self, key, *args, **kwargs):
        self.connection.zrangebyscore(name=key, *args, **kwargs)

    def zadd(self, *args, **kwargs):
        self.connection.zadd(*args, **kwargs)

    def remove(self, key=None):
        self.connection.delete(key)
