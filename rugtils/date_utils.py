'''
Some time functions
'''
import datetime
import strict_rfc3339
import calendar
import pytz

import rfc3339
from strict_rfc3339 import validate_rfc3339

from flask.ext.restful import fields


class birthdateObject(fields.Raw):
    def output(self, key, obj):
        if not isinstance(obj[key], datetime.datetime):
            datestamp = datetime.datetime.now()
        else:
            datestamp = obj[key]
        date_month = int(datestamp.strftime("%m"))
        date_day = int(datestamp.strftime("%d"))
        date_year = int(datestamp.strftime("%Y"))
        return {"month": date_month, "day": date_day, "year": date_year}


class rfc3339Stamp(fields.Raw):
    def output(self, key, obj):
        if key not in obj:
            return None
        if obj[key] is None:
            return obj[key]
        elif not isinstance(obj[key], datetime.datetime):
            if validate_rfc3339(obj[key]):
                return obj[key]
            return rfc3339.rfc3339(datetime.datetime.utcnow())

        return rfc3339.rfc3339(obj[key])


def calculate_age(born):
    if not isinstance(born, datetime.datetime):
        raise Exception('Must supply a datetime object.')
    today = datetime.datetime.today()
    return today.year - born.year -\
        ((today.month, today.day) < (born.month, born.day))


def convert_utc_to_timezone(**kwargs):
    tzoffset = kwargs.get('tzoffset', None)
    raw_date = kwargs.get('raw_date', None)
    if tzoffset is None or raw_date is None:
        raise Exception('Missing conversion parameter.')
    elif not isinstance(raw_date, datetime.datetime):
        raise Exception('Must supply a valid datetime object')
    tz = pytz.timezone(tzoffset)
    return raw_date.replace(tzinfo=pytz.utc).astimezone(tz)


def utc_convert(timestamp):
    ''' returns the timestamp in RFC 3339 format '''
    if isinstance(timestamp, datetime.datetime):
        timestamp = convert_datetime(timestamp)
    return strict_rfc3339.\
        timestamp_to_rfc3339_utcoffset(timestamp)


def rfc3339_convert(timestamp):
    if isinstance(timestamp, datetime.datetime):
        timestamp = convert_datetime(timestamp)
    return strict_rfc3339.\
        timestamp_to_rfc3339_localoffset(timestamp)


def convert_datetime(datestamp):
    return calendar.timegm(datestamp.timetuple())


def find_end_of_day(datestamp):
    tz_info = datestamp.tzinfo
    return datetime.datetime.strptime(
        datestamp.strftime(
            "%Y-%m-%d 23:59:59"),
        "%Y-%m-%d %H:%M:%S"
        ).replace(tzinfo=tz_info)


def next_day(datestamp):
    tz_info = datestamp.tzinfo
    next_day = datestamp + datetime.timedelta(days=1)
    return datetime.datetime.strptime(
        next_day.strftime(
            "%Y-%m-%d 00:00:00"),
        "%Y-%m-%d %H:%M:%S"
        ).replace(tzinfo=tz_info)


def simple_date(value):
    return datetime.datetime.strftime(value, "%Y-%m-%d %H:%M:%S")
