import math


EARTH_RADIUS = 6378.1
MIN_LAT = math.radians(-90)
MAX_LAT = math.radians(90)
MIN_LON = math.radians(-180)
MAX_LON = math.radians(180)


class GeoLocation:
    def __init__(self, latitude, longitude, distance):
        self.rad_lat = math.radians(latitude)
        self.rad_lng = math.radians(longitude)
        self.deg_lat = math.degrees(latitude)
        self.deg_lng = math.degrees(longitude)
        self.distance = distance * 1.6  # All calculations are in kilometers

    def get_range(self):
        rad_dist = self.distance / EARTH_RADIUS
        min_lat = self.rad_lat - rad_dist
        max_lat = self.rad_lat + rad_dist
        if min_lat > MIN_LAT and max_lat < MAX_LAT:
            delta_lng = math.asin(math.sin(rad_dist) / math.cos(self.rad_lat))

            min_lng = self.rad_lng - delta_lng
            if min_lng < MIN_LON:
                min_lng += 2 * math.pi

            max_lng = self.rad_lng + delta_lng
            if max_lng > MAX_LON:
                max_lng -= 2 * math.pi
        else:
            min_lat = max(min_lat, MIN_LAT)
            max_lat = min(max_lat, MAX_LAT)
            min_lng = MIN_LON
            max_lng = MAX_LON

        range = {
            'min_lat': math.degrees(min_lat),
            'max_lat': math.degrees(max_lat),
            'min_lng': math.degrees(min_lng),
            'max_lng': math.degrees(max_lng)
        }
        return range

    @staticmethod
    def calculate_distance(location, event):
        r1 = math.radians(location.latitude)
        r2 = math.radians(event['latitude'])
        lat = math.radians(event['latitude'] - location.latitude)
        lng = math.radians(event['longitude'] - location.longitude)
        a = math.sin(lat/2) * math.sin(lat/2) + \
            math.cos(r1) * math.cos(r2) * \
            math.sin(lng/2) * math.sin(lng/2)

        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        return float((EARTH_RADIUS * c)/1.6)
