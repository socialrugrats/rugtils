'''
RabbitMQ Wrapper
---
This is a small wrapper that handles connectivity to RabbitMQ
'''
import json

from amqp import Connection, Message, Channel


class RabbitWrapperError(Exception):
    pass


class RabbitWrapper:
    def __init__(self, config=None):
        self.connection = None
        self.channels = []
        self.config = config or None
        self.nodes = self.config['nodes'].split(':::')
        self.node_index = 0
        self.node_count = len(self.nodes)
        self.connect_errors = 0

    def connect(self):
        if self.node_index >= self.node_count:
            self.node_index = 0
        try:
            self.connection = Connection(
                host=self.nodes[self.node_index],
                userid=self.config['userid'],
                password=self.config['password'])
        except:
            if self.connect_errors > len(self.nodes):
                raise RabbitWrapperError(
                    'Cannot connect to the rabbitmq server')
            else:
                self.connect_errors += 1
                self.node_index += 1
                self.connect()

    def create_channel(self):
        channel = Channel(self.connection)
        self.channels.append(channel)

        return channel


class RabbitPublisher:
    def __init__(self, connection=None, exchange=None, routing_key=None):
        self.connection = connection
        self.channel = self.connection.create_channel()
        self.exchange = exchange
        self.routing_key = routing_key

    def prepare(self, message):
        return Message(message, self.channel, type="text/json")

    def publish(self, message):
        try:
            msg = self.prepare(json.dumps(message))
        except Exception as e:
            print("Error preparing message: {}".format(e))
            raise
        self.channel.basic_publish(
            msg,
            exchange=self.exchange,
            routing_key=self.routing_key)


class RabbitConsumer:
    def __init__(self, connection=None, queue=None):
        self.connection = connection
        self.channel = self.connection.create_channel()
        self.queue = queue

    def consume(self,
                queue=None,
                consumer_tag='',
                callback=None,
                no_ack=False,
                **kwargs):
        queue = queue or self.queue
        if queue is None:
            raise RabbitWrapperError('No queue present in consumer.')

        prefetch_size = kwargs.get('prefetch_size', 0)
        prefetch_count = kwargs.get('prefetch_count', 1)
        self.channel.basic_qos(
            prefetch_size,
            prefetch_count,
            False)
        self.channel.basic_consume(
            queue=queue,
            consumer_tag=consumer_tag,
            callback=callback,
            no_ack=no_ack)

        while True:
            self.channel.wait()

    def ack(self, message_tag=''):
        self.channel.basic_ack(message_tag, 1)

    def reject(self, delivery_tag='', requeue=True):
        self.channel.basic_reject(
            delivery_tag=delivery_tag,
            requeue=requeue)
