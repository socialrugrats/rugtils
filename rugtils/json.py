import json
import datetime


class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            encoded_object = datetime.\
                datetime.strftime(obj,
                                  "%a, %d %b %Y %H:%M:%S %z")
        else:
            encoded_object = json.JSONEncoder.default(self, obj)
        return encoded_object
