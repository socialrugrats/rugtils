import unittest
from unittest.mock import MagicMock, patch

from rugtils.rediswrapper import RedisWrapper


class RedisWrapperTest(unittest.TestCase):
    def setUp(self):
        mockConfig = {
            "host": "nowhere",
            "port": 1234
        }
        self.redis = RedisWrapper(mockConfig)

    def tearDown(self):
        self.redis = None

    @patch('rugtils.rediswrapper.RedisWrapper.connect')
    def test_check_connection_exception(self, connect_mock):
        connect_mock.return_value = False
        with self.assertRaises(RuntimeError):
            self.redis.check_connection()

    @patch('rugtils.rediswrapper.RedisWrapper.check_connection')
    def test_get(self, check_connection_mock):
        self.redis.connection = MagicMock()
        self.redis.connection.get.return_value = ('a', 'tuple')
        self.assertEquals(self.redis.get('fuzz'), ('a', 'tuple'))

    @patch('rugtils.rediswrapper.RedisWrapper.check_connection')
    def test_set(self, check_connection_mock):
        self.redis.connection = MagicMock()
        self.redis.connection.set.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.redis.set('foo', 'test')

    @patch('rugtils.rediswrapper.RedisWrapper.check_connection')
    def test_setex(self, check_connection_mock):
        self.redis.connection = MagicMock()
        self.redis.connection.setex.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.redis.setex('foo', 1, 'test')

    @patch('rugtils.rediswrapper.RedisWrapper.check_connection')
    def test_zrangebyscore(self, check_connection_mock):
        self.redis.connection = MagicMock()
        self.redis.connection.zrangebyscore.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.redis.zrangebyscore('foo', 'test')

    @patch('rugtils.rediswrapper.RedisWrapper.check_connection')
    def test_zadd(self, check_connection_mock):
        self.redis.connection = MagicMock()
        self.redis.connection.zadd.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.redis.zadd()

    @patch('rugtils.rediswrapper.RedisWrapper.check_connection')
    def test_remove(self, check_connection_mock):
        self.redis.connection = MagicMock()
        self.redis.connection.delete.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.redis.remove('foo')


if __name__ == '__main__':
    unittest.main()
