from pprint import pprint
import unittest
from unittest.mock import MagicMock, patch

from rugtils.rabbitmq import ( 
    RabbitWrapper,
    RabbitWrapperError,
    RabbitPublisher,
    RabbitConsumer)


class RabbitTest(unittest.TestCase):
    def setUp(self):
        self.rabbit = RabbitWrapper(
            config=MagicMock())

    def tearDown(self):
        pass

    def test_instantiate(self):
        self.assertIsInstance(self.rabbit, RabbitWrapper)

    def test_connect_exception(self):
        self.rabbit.connect_errors = 1
        with self.assertRaises(RabbitWrapperError):
            self.rabbit.connect()

    @patch('rugtils.rabbitmq.Connection')
    def test_connect(self, connection_mock):
        self.rabbit.connect_errors = 0
        self.rabbit.connect()
        self.assertIsNotNone(self.rabbit.connection)

    @patch('rugtils.rabbitmq.Channel')
    def test_create_channel(self, channel_mock):
        self.rabbit.create_channel()
        self.assertGreater(len(self.rabbit.channels), 0)


class RabbitPublisherTest(unittest.TestCase):

    @patch('rugtils.rabbitmq.Channel')
    @patch('rugtils.rabbitmq.Connection')
    def setUp(self, conn_mock, chan_mock):
        self.rabbit = RabbitWrapper(
            config=MagicMock())
        self.rabbit.connect()
        self.publisher = RabbitPublisher(
            self.rabbit,
            'test',
            'test')

    def test_instantiate(self):
        self.assertIsInstance(self.publisher, RabbitPublisher)

    @patch('rugtils.rabbitmq.Message')
    def test_prepare(self, msg_mock):
        msg_mock.return_value = True
        result = self.publisher.prepare("fake message")
        self.assertTrue(result)

    @patch('rugtils.rabbitmq.Message')
    def test_publish_exception(self, msg_mock):
        msg_mock.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.publisher.publish('fake message')

    @patch('rugtils.rabbitmq.Message')
    def test_publish(self, msg_mock):
        msg_mock.return_value = {"test": True}
        self.publisher.publish({'fake': 'message'})
        self.assertTrue(self.publisher.channel.basic_publish.called)


class RabbitConsumerTest(unittest.TestCase):
    @patch('rugtils.rabbitmq.Channel')
    @patch('rugtils.rabbitmq.Connection')
    def setUp(self, conn_mock, chan_mock):
        self.rabbit = RabbitWrapper(
            config=MagicMock())
        self.rabbit.connect()
        self.consumer = RabbitConsumer(
            self.rabbit,
            'test')

    def test_instantiate(self):
        self.assertIsInstance(self.consumer, RabbitConsumer)

    def test_ack(self):
        self.consumer.ack(1)
        self.assertTrue(self.consumer.channel.basic_ack.called)

    def test_reject(self):
        self.consumer.reject(1)
        self.assertTrue(self.consumer.channel.basic_reject.called)

    def test_consume_fail(self):
        self.consumer.queue = None
        with self.assertRaises(RabbitWrapperError):
            self.consumer.consume()

    def test_consume(self):
        self.consumer.channel.wait.side_effect = ValueError
        with self.assertRaises(ValueError):
            self.consumer.consume(
                queue='test',
                callback=MagicMock())

if __name__ == '__main__':
    unittest.main()
