import unittest

from rugtils.geocode import GeoLocation


''' Mock Location '''
class MockLocation(object):
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude


class GeocodeTest(unittest.TestCase):
    def setUp(self):
        self.location = MockLocation(-70.2928292, 123.2039292)
        self.geolocation = GeoLocation(-71.393, 122.0192, 10)

    def tearDown(self):
        self.location = None

    def test_get_range(self):
        expected_ranges = {
            'min_lat': -71.53673127925391,
            'max_lat': -71.2492687207461,
            'min_lng': 121.56873360555659,
            'max_lng': 122.46966639444342
        }
        self.assertEqual(self.geolocation.get_range(), expected_ranges)

    def test_calculate_distance(self):
        mockEvent = {
            'latitude': -70.15950,
            'longitude': 123.10115
        }
        distance = GeoLocation.calculate_distance(self.location, mockEvent)
        self.assertEqual(distance, 9.586539756719883)


if __name__ == '__main__':
    unittest.main()
