import datetime
import unittest

from rugtils.date_utils import (
    utc_convert,
    rfc3339_convert,
    calculate_age,
    convert_utc_to_timezone,
    convert_datetime,
    find_end_of_day,
    next_day,
    simple_date)


class DateUtilsTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_utc_convert(self):
        timestamp = 1422249906
        rfc_string = "2015-01-26T05:25:06Z"
        self.assertEquals(utc_convert(timestamp), rfc_string)

    def test_rfc3339_convert(self):
        timestamp = 1422249906
        rfc_string = "2015-01-26T05:25:06+00:00"
        self.assertEquals(rfc3339_convert(timestamp), rfc_string)

    def test_convert_datetime(self):
        timestamp = 1422249906
        datestamp = datetime.datetime.\
            strptime("2015-01-26 05:25:06+0000", "%Y-%m-%d %H:%M:%S%z")
        self.assertEquals(timestamp, convert_datetime(datestamp))

    def test_find_end_of_day(self):
        datestamp = datetime.datetime.\
            strptime("2015-01-26 05:25:06+0000", "%Y-%m-%d %H:%M:%S%z")
        expected = datetime.datetime.\
            strptime("2015-01-26 23:59:59+0000", "%Y-%m-%d %H:%M:%S%z")
        self.assertEqual(find_end_of_day(datestamp), expected)

    def test_next_day(self):
        datestamp = datetime.datetime.\
            strptime("2015-01-26 05:25:06+0000", "%Y-%m-%d %H:%M:%S%z")
        expected = datetime.datetime.\
            strptime("2015-01-27 00:00:00+0000", "%Y-%m-%d %H:%M:%S%z")
        self.assertEqual(next_day(datestamp), expected)

    def test_simple_date(self):
        datestamp = datetime.datetime.\
            strptime("2015-01-26 05:25:06+0000", "%Y-%m-%d %H:%M:%S%z")
        expected = datetime.datetime(
            2015,
            1,
            26,
            5,
            25,
            6).\
            strftime("%Y-%m-%d %H:%M:%S")
        self.assertEqual(simple_date(datestamp), expected)

    def test_convert_utc_to_timezone(self):
        data = {
            "raw_date": datetime.datetime.\
                strptime("2015-01-26 05:25:06+0000", "%Y-%m-%d %H:%M:%S%z"),
            "tzoffset": "America/Los_Angeles"
        }
        expected = datetime.datetime.\
            strptime("2015-01-25 21:25:06-0800", "%Y-%m-%d %H:%M:%S%z")
        self.assertEqual(convert_utc_to_timezone(**data), expected)

    def test_convert_utc_to_timezone_InvalidDateTime(self):
        data = {
            "raw_date": "fake",
            "tzoffset": "America/Los_Angeles"
        }
        with self.assertRaises(Exception):
            convert_utc_to_timezone(**data)

    def test_calculate_age(self):
        self.assertTrue(isinstance(calculate_age(datetime.datetime.
                        strptime("2015-01-26 05:25:06+0000",
                                 "%Y-%m-%d %H:%M:%S%z")), int))

    def test_calculate_age_InvalidDateTime(self):
        with self.assertRaises(Exception):
            calculate_age('this is not a datetime obj')

if __name__ == '__main__':
    unittest.main()
