import datetime
import unittest
import json

from rugtils.json import DateTimeEncoder


class JsonTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_json_with_datestamp(self):
        test_json = {
            "key": "value",
            "date": datetime.datetime(2015, 1, 27, 12)
        }
        test = json.dumps(
            test_json,
            cls=DateTimeEncoder)
        self.assertTrue('Tue, 27 Jan 2015 12:00:00 ' in test)


if __name__ == '__main__':
    unittest.main()
